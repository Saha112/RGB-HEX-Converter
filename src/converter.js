/**
 * Padding hex component if necessary.
 * Hex component representation requires
 * tow hexadecimal charactrers.
 * @param {string} comp
 * @returns {string} two hexadecimals characters.
 */
const pad = (comp) => {
    // toinen tapa // let padded = comp.length == 2 ? comp : "0" + comp
    let padded = comp;
    if(comp.length < 2) {
        padded = "0" + comp;
    }
    return padded;
}

/**
 * RGB-to-HEX conversion
 * @param {number} r RED 0-255
 * @param {number} g GREEN 0-255
 * @param {number} b BLUE 0-255
 * @returns {string} in hex color format, e.g., "#00ff00" (green)
 */

export const rgb_to_hex = (r, g, b) => {
    const HEX_RED = r.toString(16);
    const HEX_GREEN = g.toString(16);
    const HEX_BLUE = b.toString(16);
    return "#" + pad(HEX_RED) + pad(HEX_GREEN) + pad(HEX_BLUE);
};

/**
 * HEX to RGB conversion
 * @param {string} hex with 7-characters, first one '#'
 * @returns {[number, number, number]} "RED", "GREEN", "BLUE"
 * @throws Error for incorrect hex values
 */
export const hex_to_rgb = (hex) => {
    // console.log(hex);
    const raw = hex.replace('#', '');
    const rgb = [-1, -1, -1];
    try {
        if (raw.length === 6) {
            const r = parseInt(raw.slice(0,2), 16);
            const g = parseInt(raw.slice(2,4), 16);
            const b = parseInt(raw.slice(4,6), 16);
            if (!isNaN(r) && !isNaN(g) && !isNaN(b)) {
                rgb[0] = r;
                rgb[1] = g;
                rgb[2] = b;
            } else {
                throw new Error('');
            }
        } else if (raw.length === 3) {
            const r = parseInt(raw.slice(0,1).repeat(2), 16);
            const g = parseInt(raw.slice(1,2).repeat(2), 16);
            const b = parseInt(raw.slice(2,3).repeat(2), 16);
            // console.log({r,g,b});
            if (!isNaN(r) && !isNaN(g) && !isNaN(b)) {
                rgb[0] = r;
                rgb[1] = g;
                rgb[2] = b;
            } else {
                console.log(rgb);
                throw new Error('there is NaN');
            }
        }
    } catch (err) {
        console.log(err);
        throw new Error('Incorrect hex value');
    }
    return rgb;
}